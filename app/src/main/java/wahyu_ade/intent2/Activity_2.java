package wahyu_ade.intent2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Activity_2 extends Activity {
    ListView list_nama_buah;
    HashMap<Integer,Boolean> data_yg_dipilih;
    ArrayList<String> buah_dipilih;
    String[] buah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        buah = new String[6];
        buah[0] = "Pisang";
        buah[1] = "Apel";
        buah[2] = "Anggur";
        buah[3] = "Mangga";
        buah[4] = "Durian";
        buah[5] = "Pepaya";

        data_yg_dipilih = new HashMap<Integer, Boolean>();
        int i = 0;
        for(String x : buah){
            data_yg_dipilih.put(i++, false);
        }

        list_nama_buah = (ListView)findViewById(R.id.list_nama_buah);
        ArrayAdapter<String> data_buah = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, buah);
        list_nama_buah.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        list_nama_buah.setAdapter(data_buah);

        list_nama_buah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(data_yg_dipilih.get(i)){
                    data_yg_dipilih.put(i, false);
                    Toast.makeText(Activity_2.this, buah[i] + " tidak jadi dipilih", Toast.LENGTH_SHORT).show();
                }
                else{
                    data_yg_dipilih.put(i, true);
                    Toast.makeText(Activity_2.this, buah[i] + " dipilih", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        Intent data_intent_buah = getIntent();
        buah_dipilih = new ArrayList<>();
        for(Map.Entry<Integer, Boolean> value : data_yg_dipilih.entrySet()){
            if(value.getValue()){
                buah_dipilih.add(buah[value.getKey()]);
            }
        }
        data_intent_buah.putExtra("activity_2", buah_dipilih);
        setResult(1, data_intent_buah);
        finish();
    }
}

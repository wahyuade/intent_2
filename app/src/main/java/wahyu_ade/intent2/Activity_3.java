package wahyu_ade.intent2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Activity_3 extends Activity {
    GridView grid_nama_bunga;
    String[] bunga;
    ArrayList<String> bunga_dipilih;
    HashMap<Integer, Boolean> data_yg_dipilih;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);

        bunga = new String[9];
        bunga[0] = "Mawar";
        bunga[1] = "Melati";
        bunga[2] = "Kamboja";
        bunga[3] = "Sakura";
        bunga[4] = "Anggrek";
        bunga[5] = "Matahari";
        bunga[6] = "Tulip";
        bunga[7] = "Teratai";
        bunga[8] = "Sepatu";

        data_yg_dipilih = new HashMap<Integer, Boolean>();
        int i = 0;
        for(String x : bunga){
            data_yg_dipilih.put(i++, false);
        }
        ArrayAdapter<String> data_bunga = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, bunga);

        grid_nama_bunga = (GridView)findViewById(R.id.grid_nama_bunga);
        grid_nama_bunga.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        grid_nama_bunga.setAdapter(data_bunga);

        grid_nama_bunga.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(data_yg_dipilih.get(i)){
                    data_yg_dipilih.put(i, false);
                    Toast.makeText(Activity_3.this, bunga[i] + " tidak jadi dipilih", Toast.LENGTH_SHORT).show();
                }
                else{
                    data_yg_dipilih.put(i, true);
                    Toast.makeText(Activity_3.this, bunga[i] + " dipilih", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent data_intent_bunga = getIntent();
        bunga_dipilih = new ArrayList<>();
        for(Map.Entry<Integer, Boolean> value : data_yg_dipilih.entrySet()){
            if(value.getValue()){
                bunga_dipilih.add(bunga[value.getKey()]);
            }
        }
        data_intent_bunga.putExtra("activity_3", bunga_dipilih);
        setResult(2, data_intent_bunga);
        finish();
    }
}

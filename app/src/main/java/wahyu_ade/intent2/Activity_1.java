package wahyu_ade.intent2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Activity_1 extends Activity {
    Button list_buah;
    Button grid_bunga;
    ListView hasil_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        list_buah = (Button)findViewById(R.id.get_buah);
        grid_bunga = (Button)findViewById(R.id.get_bunga);
        hasil_list = (ListView)findViewById(R.id.list_output);

        list_buah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data_buah = new Intent(Activity_1.this, Activity_2.class);
                startActivityForResult(data_buah, 1);
            }
        });

        grid_bunga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data_bunga = new Intent(Activity_1.this, Activity_3.class);
                startActivityForResult(data_bunga, 2);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 1:
                ArrayAdapter<String> hasil_buah = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data.getStringArrayListExtra("activity_2"));
                hasil_list.setAdapter(hasil_buah);
                break;
            case 2:
                ArrayAdapter<String> hasil_bunga = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data.getStringArrayListExtra("activity_3"));
                hasil_list.setAdapter(hasil_bunga);
                break;
        }
    }
}